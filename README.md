
see SO thread at: http://stackoverflow.com/questions/23339907/returning-a-value-from-callback-function-in-node-js


// helper method makes async call which calls the callback 
function doCall(urlToCall, mycallback) {
    urllib.request(urlToCall, { wd: 'nodejs' }, function (err, data, response) {                              
        var statusCode = response.statusCode;
        finalData = getResponseJson(statusCode, data.toString());
        return mycallback(finalData);
    });
}

// main method
var urlToCall = "http://myUrlToCall";
doCall(urlToCall, function(response){
    // Here you have access to your variable
    // implement mycallback do something with response 
    console.log(response);
})
