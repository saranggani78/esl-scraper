/**
 * Created by cyrustalladen on 11/13/15.
 */
var fs = require('fs');
var express = require('express');
var mongoose = require('mongoose');
var app = express();

var Job = require('../jobModel');


// connect
mongoose.connect('mongodb://localhost/english_teacher_job_db', function(err){
    if(err) console.log('connection error: ', err);
    else
        console.log('connection success');
});


// insert job data using temporary output.js
var newjob = new Job({
    jid : 11111,
    title : 'test'

});

newjob.save(function(err){
   if(err) throw err;
   console.log('test job created',newjob);
});