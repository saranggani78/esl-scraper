/**
 * Created by cyrustalladen on 11/29/15.
 * reference:
 * http://thejackalofjavascript.com/architecting-a-restful-node-js-app/
 * http://mherman.org/blog/2013/11/10/social-authentication-with-passport-dot-js/#unit-tests
 */

var express = require('express');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var mongoose = require('mongoose');
var db = require('../config/database');
var User = require('../model/userModel');
var auth = require('../config/auth');
var app = express();

// serialize/ user to start session
passport.serializeUser(function(user,done){
   done(null, user);
});
// deserialize after session
passport.deserializeUser(function(obj, done){
    done(null, obj);
});
// use facebook strategy
passport.use(new FacebookStrategy({
    clientID: auth.fb.appID,
    clientSecret: auth.fb.appSecret,
    callbackURL: auth.fb.callbackURL
    },
    function(accessToken, refreshToken, profile, done){
//        console.log('profile: '+JSON.stringify(profile,null,4));
        process.nextTick(function(){
            // check if facebook user exists
            User.find({})
                .where('fb.id',profile.id)
                .exec(function(err,user){
                    if(err){
                        console.log(err);
                    }
                    if(!user.length){
                        // create new user
                        console.log('fb user not found');
                        var newUser = new User({fb:
                        {
                            id: profile.id,
                            username: profile.username,
                            displayName: profile.displayName,
                            created: Date.now(),
                            accessToken: accessToken,
                            email: profile._json.email
                        }
                        });
                        newUser.save(function(error){
                            if(error){console.log(error);
                            }else{
                                console.log('saving new facebook user ');
                                return done(null,user);
                            }
                        });
                    }
                    else{
                        // fb id exists
                        console.log('fb user exists');
                        console.log(JSON.stringify(user,null,4));
                        return done(null,user);
                    }
                });
        });

}));

// configure express middleware
// TODO install passport token
//app.use(express.session({secret:'mySecret'}));
app.use(passport.initialize());
app.use(passport.session());

// app routes
app.get('/auth/facebook',
    passport.authenticate('facebook'),
    function(req, res){
        //return user profile object
        res.send(JSON.stringify(user,null,4));
    });

app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/' }),
    function(req, res) {
        // if successfully authenticated
        res.redirect('/jobs');
    });

app.get('/',function(req,res){
    // show login options here (facebook, google)
    res.send('home page');
});

app.get('/logout', function(req, res){
    // destroy token/session
    req.logout();
    res.redirect('/');
});

app.get('/login', function(req, res){
    // login to facebook
    res.redirect('/auth/facebook');
});

// TODO login to google
app.get('/login/gplus',function(req,res){

});

app.get('/jobs', function(req, res){
    // TODO make a facebook graph call to verify user access token
    // https://graph.facebook.com/me?fields=email&access_token='+token
    // TODO if user access token is ok then send jobs object
    // TODO else prompt authentication
    // return test jobs
    res.send('entered secure page');
});

mongoose.connect(db.dbHost, function (err) {
    if (err) console.log('connection error: ', err);
    else
        console.log('connection success');
});

app.listen('8081');
console.log('go to localhost:8081/login  to check fb auth');
exports = module.exports = app;