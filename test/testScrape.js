//TODO: scrape the resource through the exposed API endpoint and test for data integrity


var fs = require('fs');
var express = require('express');
var mongoose = require('mongoose');
var app = express();

var Job = require('../jobModel');


// connect
mongoose.connect('mongodb://localhost/english_teacher_job_db', function(err){
    if(err) console.log('connection error: ', err);
    else
        console.log('connection success');
});

app.get('/test', function(){
    console.log('waiting on localhost:8081');
    // TODO: read the output-2016-03-26.json temporary file and save to database
    fs.readFile('output.json',function(err, data){
        if(err)
            console.log('error: ',err);
        data = JSON.parse(data);
        for(var i =0;i<data.length;i++){
//            console.log('data: '+JSON.stringify(data[i], null, 4));
            var newJob = new Job();
            newJob.jid = data[i].jid;
            newJob.link = data[i].link;
            newJob.title = data[i].title;
            newJob.datePosted = data[i].datePosted;
            newJob.email = data[i].email;
            newJob.poster = data[i].poster;
            newJob.description = data[i].description;
            newJob.save(function(err){});
        }
        console.log('%d objects saved ',data.length);

    });

});

app.listen('8081');
console.log('test server started on port 8081');
exports = module.exports = app;