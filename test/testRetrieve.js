/**
 * Created by cyrustalladen on 11/13/15.
 */
var mongoose = require('mongoose');

var Job = require('../jobModel');

// connect
mongoose.connect('mongodb://localhost/english_teacher_job_db', function(err){
    if(err) console.log('connection error: ', err);
    else
        console.log('connection success');
});

// get job data from db
Job.find({}, function(err, data){
    if(err) throw err;
    console.log('data: '+JSON.stringify(data, null, 4));

});
