/**
 * Created by cyrustalladen on 3/27/16.
 */
// use fs to read output files and save to db
var fs = require('fs');
var mongoose = require('mongoose');
var db = require('./config/database.js');

// TODO parse the contents of the output directory and save to db
fs.readFile('output'+dt+'.json', function (err, data) {
    if (err)
        console.log('error: ', err);

    // TODO execution crashes here if doing consecutive scrapes due to race conditions
    // TODO fix this using async
    // it will be fixed if we dont need a temporary json file
    data = JSON.parse(data);

    //TODO optimize reuse the mongodb connection if one already exists
    // connect to localhost mongodb
    mongoose.createConnection(db.localhost.dbHost, function (err) {
        if (err) console.log('connection error: ', err);
        else
            console.log('connection success');
    });

    for (var i = 0; i < data.length; i++) {
//            console.log('data: '+JSON.stringify(data[i], null, 4));
        var newJob = new Job();
        newJob.country = data[i].country;
        newJob.jid = data[i].jid;
        newJob.link = data[i].link;
        newJob.title = data[i].title;
        newJob.datePosted = data[i].datePosted;
        newJob.dateCrawled = data[i].dateCrawled;
        newJob.email = data[i].email;
        newJob.poster = data[i].poster;
        newJob.description = data[i].description.replace(/((\r)+|(\t)+|(\n)+)/g,'\\n');
        newJob.save(function (err) {
        });
    }

    mongoose.disconnect();
    console.log('Successfully saved %d objects into english_teacher_job_db.jobs ', data.length);

});