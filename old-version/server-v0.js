// resource: http://code.tutsplus.com/tutorials/screen-scraping-with-nodejs--net-25560
// scraping dave esl 
// this api returns job object with the properties
// from detail page ( poster, location, description abbreviation )
// from master page ( job id, job title, date posted ) 

/*
var phantom = require('phantom');
var ph = require('phantomjs');

phantom.create(function(ph){
	return ph.createPage(function(page){
		return page.open('http://www.eslcafe.com/jobs/korea/', function(status){
			console.log('opened dave esl cafe?', status);
			return page.evaluate(function(){
			return document.title;
			}, function(result){
			console.log('page title is'+result);
			return ph.exit();
			});		
		
		});
		
	});
});

*/

// resource: https://scotch.io/tutorials/scraping-the-web-with-node-js

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();


// set root endpoint 
/*
app.get('/',function(req,res){
	console.log('test ok');
	// return 'test ok';

})
*/

// TODO: set the /scrape endpoint which executes
// the scraping function 
app.get('/scrape', function(req,res){
  
	// target is dave esl cafe 
	url = 'http://www.imdb.com/title/tt1229340/';
	//url = 'http://www.eslcafe.com/jobs/korea/';

	request(url, function(err, res, html){
	
		
		if(err){
			console.log('there was an error in the scrape');
			//return;
		}
		

		if(!err){
			var $ = cheerio.load(html);

			var id, title, datePosted;

			var jobs = { id : "", 
				title : "",
				datePosted : "" };
			//return jobs;
		}

//	res.send('scrape complete');
	})

})

app.listen('8081')
console.log('listening in port 8081 \n please type /scrape in localhost to run');
// in node exports and module.exports reference the same object 
exports = module.exports = app;


// TODO: set the /report endpoint which lists how many has been scraped according to the targets
//


