

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();

// implementing getScrape
function getScrape(urlToCall, callback){


	// code here
	// request 1 gets the link to the job details from the master list
	// response body must be handled outside the request
	// jobList is populated with the link
	// jobList is traversed to get response data
	// response data is parsed outside of the request
	// request 1
	request(urlToCall, function(error, response, html){

		if(error){
			console.log('error: '+ error);
		}

		if(!error){
			var $ = cheerio.load(html);
			var jobLinks = [];
			$('dl dd').each(function(i, element){

				var job = $(this).children().first();
				var link = job.children().attr('href');

				jobLinks.push(link);

			}
			// sending data to callback handler
			callback(job);
			return;
		}
	}) // end request 1



}// end getScrape

app.get('/',function(req,res){

	res.send('you are at root');
});

app.get('/scrape', function(req, res){
	// Let's scrape daves esl cafe
	var url = 'http://www.eslcafe.com/jobs/china';

	var jobList = [];

	// calling getScrape and implementing the callback
	getScrape(url, function(data){

		console.log('links scraped: '+JSON.stringify(data, null, 4));
		/*
		var $$ = cheerio.load(body);

			var title = $$('p[align="CENTER"]').first().text();
			var poster = $$('p[align="CENTER"]').eq(1).find('big').text();
			var email = $$('p[align="CENTER"]').eq(1).find('a').attr('href').replace('mailto:','');

			var description = $$('p[align="CENTER"]').eq(1).first().text();

			// TODO: the date is in the first few lines of the description ,
			// search for the regex pattern after the first occurrence of "Date:"
			// TODO: pattern "DayOfWeek, 99 Month 9999, at 99:99 a.m./p.m."
			//console.log('jobList date: '+i+' '+JSON.stringify(jobList, null, 4));


*/
		/*
		var job = {
			id : job_id,
			link: job_link,
			title: job_title,
			datePosted : "",
			email : email,
			poster : poster,
			description : description
		};
		*/

	}); // end callback implementation


	/*
	fs.writeFile('output-2016-03-26.json', JSON.stringify(jobList, null, 4), function(err){
        	console.log('File successfully written! - Check your project directory for the output-2016-03-26.json file');
        })
	*/


}); // end app.get

app.listen('8081');
console.log('Magic happens on port 8081');
exports = module.exports = app;


