

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();

app.get('/',function(req,res){

	res.send('you are at root');
});

app.get('/scrape', function(req, res){
	// Let's scrape Anchorman 2
	var url = 'http://www.eslcafe.com/jobs/china';

	request(url, function(error, response, html){

		if(error){
			console.log(error);
		}

		if(!error){
			var $ = cheerio.load(html);

			var jobList = [];

			/*
			$('.header').filter(function(){
		        var data = $(this);
		        title = data.children().first().text();
		        release = data.children().last().children().text();

		        job.title = title;
		        job.release = release;

	        	})

			$('.star-box-giga-star').filter(function(){
	        	var data = $(this);
	        	rating = data.text();

	        	json.rating = rating;
	        	})
			*/

			// TODO: for each job get the id, link, title, datePosted
			$('dl dd').each(function(i, element){

				var tjob = $(this).children().first();
				var job_title = tjob.text();
				var job_id = tjob.children().attr('name');
				var job_link = tjob.children().attr('href');
				//console.log(i+' '+tjob);
				//console.log(i+' '+job_title);
				//console.log(i+' '+job_id);
				//console.log(i+' '+job_link);


				var job = {
					id : job_id,
					link: job_link,
					title: job_title,
					datePosted : "",
					email : "",
					poster : "",
					description : ""
				};


				jobList.push(job);
			});



				// TODO: make a separate request for the link, get the poster email, location, description abbrev, append to job )

			//test
			jobList.forEach(function(element, i){
				//console.log('jobList: '+i+' '+jobList[i].id+'\n');
				//console.log(JSON.stringify(jobList, null, 4));

				var turl = jobList[i].link;
				request(turl, function(err, res, htmll){
					if(err){
						console.log(err);
					}
					if(!err && res.statusCode == 200){
						//console.log('second request:'+turl);
						var $$ = cheerio.load(htmll);

						var title = $$('p[align="CENTER"]').first().text();
												var poster = $$('p[align="CENTER"]').eq(1).find('big').text();
						var email = $$('p[align="CENTER"]').eq(1).find('a').attr('href').replace('mailto:','');

						var description = $$('p[align="CENTER"]').eq(1).first().text();
						jobList[i].poster = poster;
						jobList[i].email = email;
						jobList[i].description = description;

						// TODO: the date is in the first few lines of the description , search for the regex pattern after the first occurrence of "Date:"
						// TODO: pattern "DayOfWeek, 99 Month 9999, at 99:99 a.m./p.m."
						console.log('jobList date: '+i+' '+JSON.stringify(jobList, null, 4));


					}

				});


			});
			//res.send(JSON.stringify(jobList, null, 4));

	        }

		fs.writeFile('output.json', JSON.stringify(jobList, null, 4), function(err){
        	console.log('File successfully written! - Check your project directory for the output-2016-03-26.json file');
        	})

		console.log('scrape complete');
    res.send('Check your console!')
	}) // end request



}); // end app.get

app.listen('8081');
console.log('Magic happens on port 8081');
exports = module.exports = app;

