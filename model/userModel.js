/**
 * Created by cyrustalladen on 11/29/15.
 */
var mongoose = require('mongoose');
//var bcrypt = require('bcrypt');

// jobs model & schema
var userSchema = new mongoose.Schema({
    local: {
        username: String,
        password: String
    },
    fb: {
        id: String,
        accessToken: String,
        username: String,
        displayName: String,
        email: String,
        created: Date
    },
    gplus: {
        id: String,
        accessToken: String,
        username: String,
        displayName: String,
        email: String,
        created: Date
    }
});

// setter functions

// getter functions
/*
userSchema.methods.generateHash = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}

userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.local.password);
}
*/

// export user instance
module.exports = mongoose.model('User', userSchema);