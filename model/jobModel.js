/**
 * Created by cyrustalladen on 11/10/15.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// jobs model & schema
var jobSchema = new Schema({

    /*
     country : country,
     jid : job_id,
     link: job_link,
     title: job_title,
     dateCrawled : crawlDate ,
     datePosted : "",
     email : "",
     poster : "",
     description : ""
     */
    country : String,
    jid : Number,
    link : String,
    title : String,
    dateCrawled : Date,
    datePosted : Date,
    email : String,
    poster : String,
    description : String

});

// setter functions


// getter functions


// instantiate schema for export
var Job = mongoose.model('Job', jobSchema);

// export job instance
module.exports = Job;