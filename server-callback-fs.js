// app name: Rate My English Job
// criteria: travel/vacation flexibility, boss friendliness
// kids fun level, office environment, classroom environment,
// management, money, commute, coworkers, working hours
// this endpoint exposes data to client app (ionic)
// what it does: rates english hagwon companies
// if that hagwon has a high score then the job it posts gets featured
// only positive reviews allowed to prevent unfairly bad reviews

// TODO ensure the database does not have duplicate entries
// TODO ensure the exposed endpoints are gmail/facebook authenticated

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var mongoose = require('mongoose');
var Job = require('./model/jobModel');
var User = require('./model/userModel');
var db = require('./config/database.js');
var app     = express();

// wrapper function that calls second request and return the body wrapped inside the callback
function getScrape(urlToCall, callback){

    var calledValue = request(urlToCall, function(error, response, body){

        if(error){
            console.log(error);
        }
        if(!error && response.statusCode == 200){
            calledValue = callback(body);
            return calledValue;
        }
    });
    return calledValue;

}

/*
 * client facing endpoints
 */
// authenticate the client
app.get('/login',function(){});

// after authorization redirect to here and get the latest jobs
app.get('/jobs', function(){
    // user chooses which country to choose
});

app.get('/jobs/china', function(){
    // get most current jobs in china
    // return the big data to client
});

app.get('/jobs/korea', function(){
    // get most current jobs in korea
    // return the big data to client
});

// this endpoint shows the status of the scrape resources by evaluating
// the crawl results (data integrity)
app.get('/status', function(){});

// this function removes duplicates from the database to maintain sanity
function removeDuplicates(){

}

// this function updates the database by scraping a resource and adding the results into  mongodb
// TODO the app must run a scheduled function that calls this function once per day
//app.get('/scrape/:countryName', function(req, res){
//  var country = req.params.countryName;

function scrapeESLCafe(country) {

    var dt = new Date().toISOString();
    fs.writeFileSync(__dirname +'/output/output'+dt+'.json');

    // the logic for the scrape is as tightly coupled as this hardcoded string
    var url = 'http://www.eslcafe.com/jobs/' + country.toString();

    // other websites must always have jobs
    // http://www.gooverseas.com/teaching-jobs-abroad?field_job_location_value=513

    request({
            url: url,
            headers : {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'}
        },
        function (error, response, html) {

            if (error) {
                console.log('request error: '+error);
            }
            if (!error) {
                var $ = cheerio.load(html);

                var jobList = [];
                var calledValue;

                // scrape the master list to get job id and link
                $('dl dd').each(function (i, element) {

                    var tjob = $(this).children().first();
                    var job_title = tjob.text();
                    var job_id = tjob.children().attr('name');
                    var job_link = tjob.children().attr('href');
//                var urlStr = turl.split("/");
//                var jobCountry = urlStr[urlStr.length-1];
                    var dateCrawled = new Date();

                    var job = {
                        country: country.toString(),
                        jid: job_id,
                        link: job_link,
                        title: job_title,
                        dateCrawled: dateCrawled,
                        datePosted: "",
                        email: "",
                        poster: "",
                        description: ""
                    };

                    jobList.push(job);
                });// end each()

                // iterate job master list and scrape the detail
                jobList.forEach(function (element, i, array) {

                    var turl = jobList[i].link;

                    // implement callback
                    // joblist is saved in filesystem to preserve values in callback
                    calledValue = getScrape(turl, function (body) {

                        var $$ = cheerio.load(body);
//                    var title = $$('p[align="CENTER"]').first().text();
                        var poster = $$('p[align="CENTER"]').eq(1).find('big').text();
                        var email = $$('p[align="CENTER"]').eq(1).find('a').attr('href').replace('mailto:', '');
                        var description = $$('p[align="CENTER"]').eq(1).first().text();

                        array[i].poster = poster;
                        array[i].email = email;
                        array[i].description = description;
                        // var regexStr = /(Date:)\s\b(?:(?:Mon)|(?:Tues?)|(?:Wed(?:nes)?)|(?:Thur?s?)|(?:Fri)|(?:Sat(?:ur)?)|(?:Sun))(?:day)?\b[,]?\s*\d{1,2}\s*[a-zA-Z]{3,9}\s+,?\s*\d{4}/g;
                        var regexStr = /(Date:)\s\b(?:(?:Mon)|(?:Tues?)|(?:Wed(?:nes)?)|(?:Thur?s?)|(?:Fri)|(?:Sat(?:ur)?)|(?:Sun))(?:day)?\b[,]?\s*\d{1,2}\s*[a-zA-Z]{3,9}\s+,?\s*\d{4},\s+at\s+\d{1,2}:\d{2}/g;
                        var str = description.match(regexStr);

                        if (str) {
                            str = str.toString();
                            if (str) {
                                str = str.replace('at ', '');
                                array[i].datePosted = new Date(str);
//                            console.log("date: "+array[i].datePosted);
                            }
                            else {
                                console.log('datePosted empty 2');
                            }
                        } else {
                            console.log('datePosted empty 1');
                        }

                        // joblist is accessed multiple times so this is not efficient
                        // this should ideally be ran once but the callback executes after getScrape finished and has left scope
                        fs.writeFileSync(__dirname +'/output/output'+dt+'.json', JSON.stringify(array[i], null, 4));

                        // we may now return the completed jobList
                        return 'calledValue';

                    });// end getScrape

                    // use the called value from an async function! now we can use jobList from memory instead of
                    // the temp output.json file
                    console.log('calledValue: %s %d', calledValue, i);

                });// end foreach

                console.log('Scrape complete');
                console.log('File successfully written! - Check your project directory for the output.json file');

            }

            // console.log('jobList date: '+JSON.stringify(jobList, null, 4));


        }); // end outer request

    // read the output file and push the data to mongodb
    fs.readFile(__dirname +'/output/output'+ dt +'.json', function (err, data) {
        if (err)
            console.log('error: ', err);

        // TODO execution crashes here if doing consecutive scrapes due to race conditions
        // TODO fix this using async
        // it will be fixed if we dont need a temporary json file
        data = JSON.parse(data);

        //TODO need to reuse the mongodb connection if one already exists
        // connect to localhost mongodb
        mongoose.connect(db.dbHost, function (err) {
            if (err) console.log('connection error: ', err);
            else
                console.log('connection success');
        });

        for (var i = 0; i < data.length; i++) {
//            console.log('data: '+JSON.stringify(data[i], null, 4));
            var newJob = new Job();
            newJob.country = data[i].country;
            newJob.jid = data[i].jid;
            newJob.link = data[i].link;
            newJob.title = data[i].title;
            newJob.datePosted = data[i].datePosted;
            newJob.dateCrawled = data[i].dateCrawled;
            newJob.email = data[i].email;
            newJob.poster = data[i].poster;
            newJob.description = data[i].description.replace(/((\r)+|(\t)+|(\n)+)/g,'\\n');
            newJob.save(function (err) {
            });
        }
        console.log('Successfully saved %d objects into english_teacher_job_db.jobs ', data.length);

    });
//  res.send('Check your console!');
//}); // end app.get(/scrape)

} // end function scrapeESLCafe()

//app.listen('8081');
scrapeESLCafe('korea');
//scrapeESLCafe('china')
console.log('point to localhost:8081/jobs');
exports = module.exports = app;

//TODO run a  script that makes a scheduled get request at each of the scrape endpoints once a day
// dave esl cafe china, dave korea, other china...