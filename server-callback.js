// app name: Rate My English Job
// criteria: travel/vacation flexibility, boss friendliness
// kids fun level, office environment, classroom environment,
// management, money, commute, coworkers, working hours
// this endpoint exposes data to client app (ionic)
// what it does: rates english hagwon companies
// if that hagwon has a high score then the job it posts gets featured
// only positive reviews allowed to prevent unfairly bad reviews

// TODO ensure the database does not have duplicate entries
// TODO ensure access to exposed endpoints are authenticated (gmail/facebook)

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var mongoose = require('mongoose');
var Job = require('./model/jobModel');
var User = require('./model/userModel');
var db = require('./config/database.js');
//var connection = mongoose.createConnection(db.localhost.dbHost, function (err) {
//    if (err) console.log('connection error: ', err);
//    else
//        console.log('connection success');
//});

// var agenda = require('agenda');  // for scheduling tasks
var app     = express();

//var conn = mongoose.createConnection
// client facing endpoints

// authenticate the client
app.get('/login',function(){});

// after authorization redirect to here and get the latest jobs
// user chooses which country to choose
app.get('/jobs', function(){});

// get most current jobs in china
// return the big data to client
app.get('/jobs/china', function(){});

// get most current jobs in korea
// return the big data to client
app.get('/jobs/korea', function(){});

// return the status of the scrape resources by evaluating the crawl results (data integrity)
app.get('/status', function(){});


//CORS config in server
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Access-Token');
    next();
}

// TODO CORS config in client
app.use(allowCrossDomain);

// this function removes duplicates from the database to maintain sanity
function removeDuplicates(){}

app.get('/scrape/:countryName', function(req, res){
    var country = req.params.countryName;
});


// this function updates the database by scraping a resource and adding the results into  mongodb
// TODO schedule this function once per day
function scrapeESLCafe(country) {

    // the logic for the scrape is as tightly coupled as this hardcoded string
    var url = 'http://www.eslcafe.com/jobs/' + country.toString();

    // other websites must always have jobs
    // http://www.gooverseas.com/teaching-jobs-abroad?field_job_location_value=513

    request({
            url: url,
            headers : {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'}
        },
        function (error, response, html) {
            var dt = new Date().toISOString();

            if (error) {
                console.log('request error: '+error);
            }
            if (!error && response.statusCode === 200) {
                var $ = cheerio.load(html);

                var jobList = [];
                var calledValue;

                // scrape the master job list to get id, link
                $('dl dd').each(function (i, element) {

                    var tjob = $(this).children().first();
                    var job_title = tjob.text();
                    var job_id = tjob.children().attr('name');
                    var job_link = tjob.children().attr('href');
//                  var urlStr = turl.split("/");
//                  var jobCountry = urlStr[urlStr.length-1];
                    var dateCrawled = new Date();

                    var job = {
                        country: country.toString(),
                        jid: job_id,
                        link: job_link,
                        title: job_title,
                        dateCrawled: dateCrawled,
                        datePosted: "",
                        email: "",
                        poster: "",
                        description: ""
                    };

                    // write to temp file for debugging
                    fs.writeFile(__dirname+'/output/output'+dt+'.json', JSON.stringify(jobList, null, 4),
                        function (error) {
                            if (error) {
                                console.error(error)
                            }
                        });

                    jobList.push(job);
                });// end each()

                console.log('finished outer scrape: ');

                console.log('Saving %d objects into english_teacher_job_db.jobs ', jobList.length);

                // for each item in job list, scrape to populate the details
                jobList.forEach(function (element, i, array) {

                    var turl = array[i].link;

                    // inner request for each element
                    // joblist is saved in filesystem to preserve values in callback
                    request(turl, function(error, response, body){

                        if(error){
                            console.log(error);
                        }
                        if(!error && response.statusCode === 200){

                                var $$ = cheerio.load(body);
//                              var title = $$('p[align="CENTER"]').first().text();
                                var poster = $$('p[align="CENTER"]').eq(1).find('big').text();
                                var email = $$('p[align="CENTER"]').eq(1).find('a').attr('href');
                                if(email !== undefined) email.replace('mailto:', '');
                                var description = $$('p[align="CENTER"]').eq(1).first().text();

                                if(poster) array[i].poster = poster;
                                if(email) array[i].email = email;
                                if(description) array[i].description = description;
                                // var regexStr = /(Date:)\s\b(?:(?:Mon)|(?:Tues?)|(?:Wed(?:nes)?)|(?:Thur?s?)|(?:Fri)|(?:Sat(?:ur)?)|(?:Sun))(?:day)?\b[,]?\s*\d{1,2}\s*[a-zA-Z]{3,9}\s+,?\s*\d{4}/g;
                                var regexStr = /(Date:)\s\b(?:(?:Mon)|(?:Tues?)|(?:Wed(?:nes)?)|(?:Thur?s?)|(?:Fri)|(?:Sat(?:ur)?)|(?:Sun))(?:day)?\b[,]?\s*\d{1,2}\s*[a-zA-Z]{3,9}\s+,?\s*\d{4},\s+at\s+\d{1,2}:\d{2}/g;
                                var str = description.match(regexStr);

                                if (!str) {
                                    console.log('datePosted empty 1');
                                }
                                else {
                                    str = str.toString();
                                    if (!str) {
                                        console.log('datePosted empty 2');
                                    }
                                    else {
                                        str = str.replace('at ', '');
                                        array[i].datePosted = new Date(str);
//                                      console.log("date: "+array[i].datePosted);
                                    }
                                }

                                // write to file for debugging
                                fs.writeFile(__dirname+'/output/output'+dt+'.json', JSON.stringify(jobList, null, 4),
                                    function (error) {
                                        if (error) {
                                            console.error(error)
                                        }
                                    });

                                // TODO: save to database
                                mongoose.connect(db.localhost.dbHost, function (err) {
                                        if (err) console.log('connection error: ', err);
                                            else
                                        console.log('connection success');
                                });
//                              console.log('job info: '+JSON.stringify(array[i], null, 4));
                                var newJob = new Job();
                                newJob.country = element.country;
                                newJob.jid = element.jid;
                                newJob.link = element.link;
                                newJob.title = element.title;
                                newJob.datePosted = element.datePosted;
                                newJob.dateCrawled = element.dateCrawled;
                                newJob.email = element.email;
                                newJob.poster = element.poster;
                                newJob.description = element.description.replace(/((\r)+|(\t)+|(\n)+)/g,'\\n');
                                newJob.save(function (err) { });

                                mongoose.disconnect();

                        }
                    });// end inner request

                });// end foreach item in joblist


            }
        }); // end outer request

};// end scrapeESLCafe()

//app.listen('8081');
//scrapeESLCafe('korea');
scrapeESLCafe('china')
console.log('point to localhost:8081/jobs');
exports = module.exports = app;

//TODO run a  script that makes a scheduled get request at each of the scrape endpoints once a day
// dave esl cafe china, dave korea, other china...